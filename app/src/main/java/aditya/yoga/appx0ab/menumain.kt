package aditya.yoga.appx0ab

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.mainmenu.*

class menumain : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.mainmenu)
        btnmhs1.setOnClickListener(this)
        btnprodi1.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnmhs1->{
                val pindah =  Intent(this,MainActivity::class.java)
                startActivity(pindah)
            }
            R.id.btnprodi1->{
                val pindah = Intent(this,kategori::class.java)
                startActivity(pindah)

            }
        }
    }
}